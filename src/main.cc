/******************************************************************************
 * myAVR-MK3_cpp-interrupt-example
 * =====================
 *
 *   -  myAVR-MK3 - C++ interrupt example implementation
 *
 * Copyright (c) 2017 Martin Singer <martin.singer@web.de>
 *
 * This file is part of MyAVR-MK3_KMX-TWI-INT
 *
 *    myAVR-MK3_cpp-interrupt-example is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    myAVR-MK3_cpp-interrupt-example is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with myAVR-MK3_cpp-interrupt-example.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/


/// myAVR-MK3 - C++ interrupt example implementation
///
/// @file main.cc
/// @author Martin Singer


#include <avr/io.h>
#include "mk3/led.h"
#include "interrupt.h"


int main(void)
{
//	volatile uint8_t prescaler = CLKPR;

	MK3::Led leds;
	Timer timer;

	sei();

	while (1) {
//		leds.writePort(~prescaler);
		leds.writePort(timer.counter);
	}

	cli();
	return 0;
}

