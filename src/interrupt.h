/******************************************************************************
 * myAVR-MK3_cpp-interrupt-example
 * =====================
 *
 *   -  myAVR-MK3 - C++ interrupt example implementation
 *
 * Copyright (c) 2017 Martin Singer <martin.singer@web.de>
 *
 * This file is part of MyAVR-MK3_KMX-TWI-INT
 *
 *    myAVR-MK3_cpp-interrupt-example is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    myAVR-MK3_cpp-interrupt-example is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with myAVR-MK3_cpp-interrupt-example.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/


/// myAVR-MK3 - C++ interrupt example implementation
///
/// @file interrupt.h
/// @author Martin Singer


#ifndef INTERRUPT_H
#define INTERRUPT_H


#ifndef F_CPU
#define F_CPU 16000000ul
#endif


#include <avr/io.h>
#include <avr/interrupt.h>


/*
 * For service routine vector numbers (e.g. ATmega2560) have a look at following files:
 * * /usr/avr/include/avr/iom2560.h
 * * /usr/avr/include/avr/iomxx0_1.h
 */
class Timer {
	public:
		Timer();

		volatile uint8_t counter;

	protected:
		class TimerInterrupt {
			public:
				static void record(Timer*);

			protected:
				static Timer *ownerTimer;
				static void serviceRoutine()
					__asm__("__vector_32") ///< TIMER3_COMPA_vect
					__attribute__((__signal__, __used__, __externally_visible__));
		};

	friend Timer::TimerInterrupt;
};


#endif // INTERRUPT_H

