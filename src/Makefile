# myAVR-MK3_KMX-TWI-INT
# =====================
#
#   - myAVR-MK3 Key Matrix on TWI with Interrupt
#
# Copyright (c) 2017 Martin Singer <martin.singer@web.de>
#
# This file is part of myAVR-MK3_KMX-TWI-INT.
#
#
# About this Makefile
# -------------------
#
# WinAVR makefile written by Eric B. Weddington, Jörg Wunsch, et al.
# Released to the Public Domain.
# Please read the make user manual!
#
# Additional material for this makefile was submitted by:
# * Tim Henigan
# * Peter Fleury
# * Reiner Patommel
# * Sander Pool
# * Frederik Rouleau
# * Markus Pfaff
# * Martin Singer
#
#
# Usage
# -----
#
# * `make all`        Make software.
# * `make clean`      Clean out built project files.
# * `make coff`       Convert ELF to AVR COFF (for use with AVR Studio 3.x or VMLAB).
# * `make extcoff`    Convert ELF to AVR Extended COFF (for use with AVR Studio
#                     4.07 or greater).
# * `make program`    Download the hex file to the device, using avrdude.  Please
#                     customize the avrdude settings below first!
# * `make filename.s` Just compile filename.c into the assembler code only
#
# To rebuild project do `make clean` then `make all`.
#
#
# Information
# -----------
#
#	$ man avr-gcc
#	$ avrdude -c


## Microcontroller Unit.
MCU = atmega2560


## Central Processing Unit frequency.
# (used for define F_CPU in all assembler and C sources)
F_CPU = 16000000ul


## Output fromat.
# [srec, ihex, binary]
FORMAT = ihex


## Target filename.
TARGET = fw


## List of extra directories to look for include and link files.
EXTRADIRS = mk3


## List of C source files.
CSRC =


## List of C++ source files.
CXXSRC = \
         mk3/led.cc \
         interrupt.cc \
         main.cc


## List of Assembler source files.
# (use .S, don't use .s for file extension)
ASRC =


## Optimization level.
# [0, 1, 2, 3, s]
# (see avr-libc FAQ)
OPT = s


## Debugging format.
# Native formats for AVR-GCC's -g are `stabs` (default) or `dwarf-2`.
# AVR (extended) `coff` requires stabs, plus an avr-objcopy run.
# `stabs+` uses GNU extensions, understood only by the GNU debugger (GDB).
DEBUG = stabs+


## C Standard.
CSTANDARD = -std=gnu11


## C++ Standard.
CXXSTANDARD = -std=c++14


## -D or -U options.
#CDEFS = -DTEST
CDEFS =


## -I options.
CINCS =


## C Compiler flags.
#  -g*:          generate debugging information
#  -O*:          optimization level
#  -f...:        tuning, see GCC manual and avr-libc documentation
#  -Wall...:     warning level
#  -Wa,...:      tell GCC to pass this to the assembler.
#    -adhlns...: create assembler listing
CFLAGS  = -g$(DEBUG)
CFLAGS += $(CDEFS)
CFLAGS += $(CINCS)
CFLAGS += -O$(OPT)
CFLAGS += -funsigned-char
CFLAGS += -funsigned-bitfields
CFLAGS += -fpack-struct
CFLAGS += -fshort-enums
CFLAGS += -Wall
CFLAGS += -Wstrict-prototypes
CFLAGS += -Wa,-adhlns=$(<:.c=.lst)
CFLAGS += $(patsubst %,-I%,$(EXTRADIRS))
CFLAGS += $(CSTANDARD)
CFLAGS += -DF_CPU=$(F_CPU)


## C++ Compiler flags.
CXXFLAGS  = -g$(DEBUG)
CXXFLAGS += $(CDEFS)
CXXFLAGS += $(CINCS)
CXXFLAGS += -O$(OPT)
#CXXFLAGS += -fpermissive
CXXFLAGS += -funsigned-char
CXXFLAGS += -funsigned-bitfields
CXXFLAGS += -fpack-struct
CXXFLAGS += -fshort-enums
CXXFLAGS += -Wall
CXXFLAGS += -Wa,-adhlns=$(<:.cc=.lst)
CXXFLAGS += $(patsubst %,-I%,$(EXRADIRS))
CXXFLAGS += $(CXXSTANDARD)
CXXFLAGS += -DF_CPU=$(F_CPU)


## Assembler flags.
#  -Wa,...:   tell GCC to pass this to the assembler.
#    -ahlms:  create listing
#    -gstabs: have the assembler create line number information; note that
#             for use in COFF files, additional information about filenames
#             and function names needs to be present in the assembler source
#             files -- see avr-libc docs [FIXME: not yet described there]
ASFLAGS  = -Wa,-adhlns=$(<:.S=.lst),-gstabs
ASFLAGS += -DF_CPU=$(F_CPU)


## Aditional libraries.

# Minimalistic printf version
PRINTF_LIB_MIN = -Wl,-u,vfprintf -lprintf_min

# Floating point printf version
# (requires MATH_LIB = -lm below)
PRINTF_LIB_FLOAT = -Wl,-u,vfprintf -lprintf_flt

# Minimalistic scanf version
SCANF_LIB_FLOAT = -Wl,-u,vfscanf -lscanf_min

# Floating point + %[ scanf version
# (requires MATH_LIB = -lm below)
SCANF_LIB_FLOAT = -Wl,-u,vfscanf -lscanf_flf

PRINTF_LIB =
SCANF_LIB =
MATH_LIB =
#MATH_LIB = -lm


# External memory options

# 64 KB of external RAM, starting after internal RAM (ATmega128!),
# used for variables (.data/.bss) and heap (malloc()).
#EXTMEMOPTS = -Wl,-Tdata=0x801100,--defsym=__heap_end=0x80ffff

# 64 KB of external RAM, starting after internal RAM (ATmega128!),
# only used for heap (malloc()).
#EXTMEMOPTS = -Wl,--defsym=__heap_start=0x801100,--defsym=__heap_end=0x80ffff

EXTMEMOPTS =


## Linker flags.
#   -Wl,...:  tell GCC to pass this to linker
#     -Map:   create map file
#     --cref: add cross reference to map file
LDFLAGS  = -Wl,-Map=$(TARGET).map,--cref
LDFLAGS += $(EXTMEMOPTS)
LDFLAGS += $(PRINTF_LIB) $(SCANF_LIB) $(MATH_LIB)


# Programming support using `avrdude`
# -----------------------------------

## Programming hardware.
# Type: `avrdude -c` to get a full listing.
#
# * `stk500v2`: MySmartUSB MK3
# * `avr911`:   AVR USB Bootloader (e.g. ATmega32U4)
AVRDUDE_PROGRAMMER = stk500v2
#AVRDUDE_PROGRAMMER = avr911


## Programmer ingerface port.
AVRDUDE_PORT = /dev/ttyUSB0


AVRDUDE_WRITE_FLASH = -U flash:w:$(TARGET).hex
#AVRDUDE_WRITE_EEPROM = -U eeprom:w:$(TARGET).eep

#AVRDUDE_WRITE_FLASH = -U flash:w:$(TARGET).hex:i
#AVRDUDE_WRITE_EEPROM = -U eeprom:w:$(TARGET).eep:i


# Uncomment the following if you want avrdude's erase cycle counter.
# Note that this counter needs to be initialized first using -Yn,
# see avrdude manual.
#AVRDUDE_ERASE_COUNTER = -y


# Uncomment the following if you do /not/ wish a verification to be
# performed after programming the device.
#AVRDUDE_NO_VERIFY = -V


## Skip Device Signature Check.
# (when signature is broken - erased or overwritten)
AVRDUDE_SKIP_SIGNATURE = -F


# Increase verbosity level.  Please use this when submitting bug
# reports about avrdude. See <http://savannah.nongnu.org/projects/avrdude>
# to submit bug reports.
AVRDUDE_VERBOSE = -v -v
#AVRDUDE_VERBOSE = -v -v -v -v


AVRDUDE_FLAGS = -p $(MCU) -P $(AVRDUDE_PORT) -c $(AVRDUDE_PROGRAMMER)
AVRDUDE_FLAGS += $(AVRDUDE_NO_VERIFY)
AVRDUDE_FLAGS += $(AVRDUDE_SKIP_SIGNATURE)
AVRDUDE_FLAGS += $(AVRDUDE_VERBOSE)
AVRDUDE_FLAGS += $(AVRDUDE_ERASE_COUNTER)


# ---------------------------------------------------------------------


## Define directories, if needed.
DIRAVR = /opt/cross
DIRAVR = $(DIRAVR)/bin
DIRAVRUTILS = $(DIRAVR)/utils/bin
DIRINC = $(DIRAVR)/avr/include
DIRLIB = $(DIRAVR)/avr/lib


## Define programs and commands.
SHELL = sh
#CC = avr-gcc
CXX = avr-g++
OBJCOPY = avr-objcopy
OBJDUMP = avr-objdump
SIZE = avr-size
NM = avr-nm
AVRDUDE = avrdude
REMOVE = rm -f
COPY = cp


## Define messages.
MSG_ERRORS_NONE = Errors none
MSG_BEGIN = -------- begin --------
MSG_END = --------  end  --------
MSG_SIZE_BEFORE = Size bevore: 
MSG_SIZE_AFTER = Size after: 
MSG_COFF = Converting to AVR COFF: 
MSG_EXTENDED_COFF = Converting to AVR extended COFF: 
MSG_FLASH = Creating load file for flash: 
MSG_EEPROM = Creating load file for EEPROM: 
MSG_EXTENDED_LISTING = Creating extended listing: 
MSG_SYMBOL_TABLE = Creating symbol table: 
MSG_LINKING = Linking: 
MSG_COMPILING = Compiling: 
MSG_ASSEMBLING = Assembling: 
MSG_CLEANING = Cleaning: 


## Define all object files.
OBJ = $(CSRC:.c=.o) $(CXXSRC:.cc=.o) $(ASRC.S=o)


## Define all listing files.
LST = $(CSRC:.c=.lst) $(CXXSRC:.cc=.lst) $(ASRC:.S=.lst)


## Compiler flags to generate dependency files.
#GENDEPFLAGS = -Wp,-M,-MP,-MT,$(*F).o,-MF,.dep/$(@F).d
GENDEPFLAGS = -MD -MP -MF .dep/$(@F).d


## Combine all necessary flags and optional flags.
# Add target processor to flags
ALL_CFLAGS = -mmcu=$(MCU) -I. $(CFLAGS) $(GENDEPFLGS)
ALL_CXXFLAGS = -mmcu=$(MCU) -I. $(CXXFLAGS) $(GENDEPFLGS)
ALL_ASFLAGS = -mmcu=$(MCU) -I. -x assembler-with-cpp $(ASFLAGS)


## Default target.
all: begin gccversion sizebefore build sizeafter finished end

build: elf hex epp lss sym

elf: $(TARGET).elf
hex: $(TARGET).hex
epp: $(TARGET).eep
lss: $(TARGET).lss
sym: $(TARGET).sym


## Messages.
begin:
	@echo
	@echo $(MSG_BEGIN)

finished:
	@echo $(MSG_ERRORS_NONE)

end:
	@echo $(MSG_END)
	@echo


## Display size of file.
HEXSIZE = $(SIZE) --target=$(FORMAT) $(TARGET).hex
ELFSIZE = $(SIZE) -A $(TARGET).elf

sizebefore:
	@if [ -f $(TARGET).elf ]; then echo; echo $(MSG_SIZE_BEFORE); $(ELFSIZE); echo; fi

sizeafter:
	@if [ -f $(TARGET).elf ]; then echo; echo $(MSG_SIZE_AFTER); $(ELFSIZE); echo; fi


## Display compiler version information.
gccversion:
	@$(CXX) --version


## Programm the device.
program: $(TARGET).hex $(TARGET).eep
	$(AVRDUDE) $(AVRDUDE_FLAGS) $(AVRDUDE_WRITE_FLASH) $(AVRDUDE_WRITE_EEPROM)


## Convert ELF to COFF for use in debugging / simulating in AVR Studio or VMLAB.
COFFCONVERT = $(OBJCOPY) --debugging \
	--change-section-address .data-0x800000 \
	--change-section-address .bss-0x800000 \
	--change-section-address .noinit-0x800000 \
	--change-section-address .eeprom-0x810000

coff: $(TARGET).elf
	@echo
	@echo $(MSG_COFF) $(TARGET).cof
	$(COFFCONVERT) -O coff-avr $< $(TARGET).cof

extcoff: $(TARGET).elf
	@echo
	@echo $(MSG_EXTENDED_COFF) $(TARGET).cof
	$(COFFCONVERT) -O coff-ext-avr $< $(TARGET).cof


## Create final output files (.hex, .eep) from ELF output file.
%.hex: %.elf
	@echo
	@echo $(MSG_FLASH) $@
	$(OBJCOPY) -O $(FORMAT) -R .eeprom $< $@

%.eep: %.elf
	@echo
	@echo $(MSG_FLASH) $@
	$(OBJCOPY) -j .eeprom --set-section-flags=.eeprom="alloc,load" \
	--change-section-lma .eeprom=0 -O $(FORMAT) $< $@


## Create extended listing file from ELF output file.
%.lss: %.elf
	@echo
	@echo $(MSG_EXTENDED_LISTING) $@
	$(OBJDUMP) -h -S $< > $@


## Create a symbol table from ELF output file.
%.sym: %.elf
	@echo
	@echo $(MSG_SYMBOL_TABLE) $@
	$(NM) -n $< > $@


## Link: create ELF output file from object files.
.SECONDARY : $(TARGET).elf
.PRECIOUS : $(OBJ)
%.elf: $(OBJ)
	@echo
	@echo $(MSG_LINKING) $@
	$(CXX) $(ALL_CXXFLAGS) $(OBJ) --output $@ $(LDFLAGS)


## Compile: create object files from C source files.
%.o : %.c
	@echo
	@echo $(MSG_COMPILING) $<
	$(CXX) -c $(ALL_CFLAGS) $< -o $@


## Compile: create assembler files from C source files.
%.s : %.c
	$(CXX) -S $(ALL_CFLAGS) $< -o $@


## Compile: create object files from C++ source files.
%.o : %.cc
	@echo
	@echo $(MSG_COMPILING) $<
	$(CXX) -c $(ALL_CXXFLAGS) $< -o $@


## Compile: create assembler files from C++ source files.
%.s : %.cc
	$(CXX) -S $(ALL_CXXFLAGS) $< -o $@


## Assemble: create object files from assembler source files.
%.o : %.S
	@echo
	@echo $(MSG_ASSEMBLING) $<
	$(CXX) -c $(ALL_ASFLAGS) $< -o $@


## Target: clean project.
clean: begin clean_list finished end

clean_list:
	@echo
	@echo $(MSG_CLEANING)
	$(REMOVE) $(TARGET).hex
	$(REMOVE) $(TARGET).eep
	$(REMOVE) $(TARGET).obj
	$(REMOVE) $(TARGET).cof
	$(REMOVE) $(TARGET).elf
	$(REMOVE) $(TARGET).map
	$(REMOVE) $(TARGET).obj
	$(REMOVE) $(TARGET).a90
	$(REMOVE) $(TARGET).sym
	$(REMOVE) $(TARGET).lnk
	$(REMOVE) $(TARGET).lss
	$(REMOVE) $(OBJ)
	$(REMOVE) $(LST)
	$(REMOVE) $(CSRC:.c=.s)
	$(REMOVE) $(CSRC:.c=.d)
	$(REMOVE) $(CXXSRC:.cc=.s)
	$(REMOVE) $(CXXSRC:.cc=.d)
	$(REMOVE) .dep/*


## Include the dependency files.
-include $(shell mkdir .dep 2>/dev/null) $(wildcard .dep/*)


## Listing of phony targets.
.PHONY : all begin finish end sizebefore sizeafter gccversion \
build elf hex eep lss sym coff extcoff \
clean clean_list program

